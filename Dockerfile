FROM node:alpine

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY public public/
COPY src src/

RUN yarn build 

RUN ls -l
RUN rm -rf src

RUN yarn add serve -g

EXPOSE 3004

CMD ["yarn", "start","serve"]